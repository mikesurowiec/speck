
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: 'Speck' })
};

exports.about = function(req, res){
  res.render('about', { title: 'Speck' })
};

exports.bigscreen = function(req, res){
  res.render('bigscreen', { title: 'Speck' })
};
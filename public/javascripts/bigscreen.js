$(function(){

  var socket = io.connect(window.location.hostname+":3000");
  var points = [];

  socket.on('news', function (data) {
    console.log(data.hello);
    socket.emit('my other event', { my: 'data' });
  });

  // acceleration reading
  socket.on('devicemotion', function (data){
    //translate the element

  });

  // gyroscope reading
  socket.on('deviceorientation', function (data){
    //rotate the element
    //console.log("alpha: " + data.alpha);
    $("#box").css("-webkit-transform", "rotateY("+data.alpha+"deg) rotateX("+data.beta+"deg)");
  });

  socket.on('touchstart', function (touches){
    points = touches;
  });

  socket.on('touchmove', function (touches){
    //$("#readerFrame").trigger('touchmove');
    // var evt;
    // try {
    //   evt = document.createEvent('TouchEvents');
    //   evt.initTouchEvent('touchstart', true, true);
    // } catch (e){
    //   evt = document.createEvent('MouseEvents'); //non touch enabled browser
    //   evt.initMouseEvent('mousedown', true, true);
    // }
    // var targetElement = document.elementFromPoint(touches[0].x, touches[0].y);

    // event.page = {
    //   x: touches[0].x,
    //   y: touches[0].y
    // };
     

    // evt.view = window;
    // evt.altKey = false;
    // evt.ctrlKey = false;
    // evt.shiftKey = false;
    // evt.metaKey = false;

    // targetElement.dispatchEvent(event);

    points = touches;
  });

  socket.on('touchend', function(touches){
    points = []; //clear the points
  });

window.requestAnimFrame = (function(callback){
    return window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function(callback){
        window.setTimeout(callback, 1000 / 30);
    };
})();
 
function animate(){
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    //console.log('animating')
 
    // update
 
    // clear
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    
    var xSum = 0, ySum = 0;
    var length = points.length;
    for(var i=0; i<length; i++){
      xSum += points[i].x;
      ySum += points[i].y;
    }
    var centroid = { x: (xSum/length), y: (ySum/length) };

    // draw each point
    for(var i=0; i<points.length; i++){
      var tx = points[i].x;
      var ty = points[i].y;

        //draw line
        
        if( points.length > 1){

          ctx.beginPath();

          if(i == points.length-1){
            ctx.moveTo(points[0].x, points[0].y);
          } else {
            ctx.moveTo(points[i+1].x, points[i+1].y);
          }
            ctx.lineTo(tx, ty);
            ctx.strokeStyle = "rgba(245,200,255,1)";
            ctx.lineWidth = 2;
            ctx.stroke();
        }

        //draw line to centroid
        // ctx.beginPath();
        // ctx.moveTo(centroid.x, centroid.y);
        // ctx.lineTo(tx, ty);
        // ctx.strokeStyle = "rgba(245,200,255,1)";
        // ctx.lineWidth = 2;
        // ctx.stroke();
        

        //draw circle
        ctx.beginPath();
        ctx.arc(tx,ty,15,0,Math.PI*2,true);
        ctx.closePath();
        ctx.fillStyle = "rgba(245,245,255,1)";
        ctx.fill();

        //draw outer circle
        // ctx.beginPath();
        // ctx.arc(tx,ty,24,Math.PI*2,true);
        // ctx.closePath();
        // ctx.strokeStyle = "rgba(245,245,255,1)";
        // ctx.lineWidth = 1;
        // ctx.stroke();

        

      // draw circle at touch point
      // ctx.fillStyle="rgba(255,255,255,.2)";
      // ctx.strokeStyle="rgba(255,255,255,.2)";
      // ctx.lineWidth = 3;

      //   ctx.beginPath();  
      //   ctx.arc(tx,ty,20,0,Math.PI*2,true);
      //   ctx.closePath();
      //   ctx.fill(); 

      //   ctx.beginPath();
      //   ctx.moveTo(100, 100);
      //   ctx.lineTo(tx, ty); 
        //ctx.lineTo(centroid.x, centroid.y);
        
        //ctx.fill(); 
        //ctx.stroke(); 
      
      // for (var i=0;i<4;i++){  
      //   ctx.beginPath();  
      //   ctx.arc(tx,ty,10+10*i,0,Math.PI*2,true);  
      //   ctx.closePath();
      //   ctx.fill();  
      // }  

      // draw line to centroid
      // ctx.beginPath();
      // ctx.moveTo(centroid.x, centroid.y);
      // ctx.lineTo(tx, ty);
      // ctx.closePath();
      // ctx.stroke();

    }
 
    // request new frame
    requestAnimFrame(function(){
        animate();
    });
}

function getCentroid(coords){
  var xSum, ySum;
  var length = coords.length;

  for(var i=0; i<length; i++){
    xSum += coords[i].x;
    ySum += coords[i].y;
  }

  return { x: xSum/length, y: ySum/length };
}

// try {
//     document.createEvent("TouchEvent");
//   } catch(e) {
//     return;
//   }
 
//   ['touchstart', 'touchmove', 'touchend'].each(function(type){
//       Element.NativeEvents[type] = 2;
//   });
 
//   var mapping = {
//     'mousedown': 'touchstart',
//     'mousemove': 'touchmove',
//     'mouseup': 'touchend'
//   };
 
//   var condition = function(event) {
//     var touch = event.event.changedTouches[0];
//     event.page = {
//       x: touch.pageX,
//       y: touch.pageY
//     };
//     return true;
//   };
 
//   for (var e in mapping) {
//     Element.Events[e] = {
//       base: mapping[e],
//       condition: condition
//     };
//   }

  // try {
  //   var evt = document.createEvent("TouchEvent");
  //   console.log(evt);
  // } catch(e) {
  //   console.log(e);
  // }

animate();

});
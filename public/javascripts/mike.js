$(function(){

  var socket = io.connect(window.location.hostname + ":3000");
  socket.on('news', function (data) {
    console.log(data.hello);
    socket.emit('my other event', { my: 'data' });
  });

  $("#area").css("-webkit-backface-visibility", "hidden");
  $("#area").css("-webkit-transform-origin", "50%, 50%");
  var offsetX;
  var offsetY;

  $('body').on('touchmove touchstart', function(e){
    e.originalEvent.preventDefault();
  });

  $("#area").on("touchstart", function(e){
    e.preventDefault();
    var touches = e.originalEvent.touches;

    var touchArr = [];
    for(var x=0; x < touches.length; x++){
      touchArr.push({x: touches[x].pageX, y: touches[x].pageY});
    }

    socket.emit('touchstart', touchArr);
  });

  $("#area").on("touchmove", function(e){
    e.preventDefault();
    var touches = e.originalEvent.touches;

    var touchArr = [];
    for(var x=0; x < touches.length; x++){
      touchArr.push({x: touches[x].pageX, y: touches[x].pageY});
    }

    socket.emit('touchmove', touchArr);
  });

  $("#area").on("touchend", function(e){
    e.preventDefault();
    var touches = e.originalEvent.touches;

    var touchArr = [];
    for(var x=0; x < touches.length; x++){
      touchArr.push({x: touches[x].pageX, y: touches[x].pageY});
    }

    socket.emit('touchend', touchArr);
  });

    // gyroscope readings if available
    window.ondeviceorientation = function(event) {
      var data = { alpha: event.alpha,
                   beta:  event.beta,
                   gamma: event.gamma };

      socket.emit('deviceorientation', data)
    }

    // accelerometer readeings if available
    window.ondevicemotion = function(event) {
      var data = { x: event.accelerationIncludingGravity.x,
                   y: event.accelerationIncludingGravity.y,
                   z: event.accelerationIncludingGravity.z };

      socket.emit('devicemotion', data);
    }


  socket.on('touchmove', function(touches){
    $("#coords").html('');
    for(var x=0; x < touches.length; x++){
    	$("#coords").append("x: " + touches[x].x + " y: " + touches[x].y + "<br />");
    }
  });


  // onTouchStart: function(e) {

  //     // remember the initial touch point
  //     touchStart = { x: e.touches.get(0).position.x, y: e.touches.get(0).position.y };

  //     // remember the element's starting point
  //     elPositionStart = { x: this.el.offsetLeft, y: this.el.offsetTop };
  //   },
  //   onTouchMove: function(e) {

  //     e.originalEvent.preventDefault();
  //     touchMove = { x: e.touches.get(0).position.x, y: e.touches.get(0).position.y };

  //     var dx = touchMove.x - touchStart.x;
  //     var dy = touchMove.y - touchStart.y;
       
  //     // apply transformation
  //     this.el.style.webkitTransform = "translate3d(" + dx + "px, " + dy + "px, 0)";

  //     //transformEl.call(this, ratio);
  //   },
  //   onTouchEnd: function(e) {
    
  //      //var target = e.touches.get(0).target;
  //      touchEnd = { x: e.touches.get(0).position.x, y: e.touches.get(0).position.y };

  //      //adjust the final position and remove the webkit transform
  //      this.el.style.position = "absolute";
  //      this.el.style.left = elPositionStart.x + (touchEnd.x - touchStart.x) + "px";
  //      this.el.style.top = elPositionStart.y + (touchEnd.y - touchStart.y) + "px";

  //      this.el.style.webkitTransform = "translate3d(0,0,0)";

  //   },


});
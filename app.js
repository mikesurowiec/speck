
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes');

var app = module.exports = express.createServer();
var io = require('socket.io').listen(app);
// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser());
  app.use(express.session({ secret: 'your secret here' }));
  app.use(require('stylus').middleware({ src: __dirname + '/public' }));
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Routes

app.get('/', routes.index);
app.get('/about', routes.about);
app.get('/bigscreen', routes.bigscreen);

app.listen(3000, function(){
  console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});

io.sockets.on('connection', function (socket) {
  socket.emit('news', { hello: 'world' });
  socket.on('my other event', function (data) {
    console.log(data);
  });

  socket.on('touchmove', function(touches){
    io.sockets.emit('touchmove', touches);
  });

  socket.on('touchstart', function(touches){
    io.sockets.emit('touchstart', touches);
  });

  socket.on('touchend', function(touches){
    io.sockets.emit('touchend', touches);
  });

  socket.on('devicemotion', function(data){
    io.sockets.emit('devicemotion', data);
  });
  
  socket.on('deviceorientation', function(data){
    io.sockets.emit('deviceorientation', data);
  });


});
